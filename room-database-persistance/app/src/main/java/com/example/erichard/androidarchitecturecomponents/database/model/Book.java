package com.example.erichard.androidarchitecturecomponents.database.model;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(
        entity = User.class, parentColumns = "id", childColumns = "user_id"
))
public class Book {
    @PrimaryKey
    public int id;
    public String title;

    @ColumnInfo(name = "user_id")
    private int user;

    public Book() {
    }

    public Book(int id, String title, int user) {
        this.id = id;
        this.title = title;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
