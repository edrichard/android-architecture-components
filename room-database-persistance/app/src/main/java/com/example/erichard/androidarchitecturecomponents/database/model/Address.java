package com.example.erichard.androidarchitecturecomponents.database.model;

import android.arch.persistence.room.ColumnInfo;

/**
 * Created by erichard on 25/11/2017.
 */

public class Address {
    private String street;
    private String state;
    private String city;

    @ColumnInfo(name = "zip_code")
    private int zipCode;

    public Address() {
    }

    public Address(String street, String state, String city, int zipCode) {
        this.street = street;
        this.state = state;
        this.city = city;
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }
}
