package com.example.erichard.androidarchitecturecomponents;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.erichard.androidarchitecturecomponents.database.model.User;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private final List<User> list;

    UserAdapter(List<User> list) {
        this.list = list;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView gender;
        private ImageView picture;

        private Context context;

        UserViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.user_name);
            gender = itemView.findViewById(R.id.user_gender);
            picture = itemView.findViewById(R.id.user_picture);

            context = itemView.getContext();
        }

        @SuppressLint("SetTextI18n")
        void bind(User user) {
            name.setText(user.getLastname() + " " + user.getFirstname());
            gender.setText(user.getGender());




            Glide.with(getContext()).load(user.getPicture()).into(picture);
        }

        Context getContext() {
            return context;
        }
    }
}
