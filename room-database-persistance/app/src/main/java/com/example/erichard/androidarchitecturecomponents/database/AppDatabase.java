package com.example.erichard.androidarchitecturecomponents.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.erichard.androidarchitecturecomponents.database.model.User;


@Database(entities = {User.class}, version = 1)
//@TypeConverter({DateTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public static final String DB_NAME = "app_db";
    public abstract UserDao userDao();

}
