package com.example.erichard.androidarchitecturecomponents.database.model;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "user")
public class User {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "last-name")
    private String lastname;

    @ColumnInfo(name = "first-name")
    private String firstname;

    @ColumnInfo(name = "picture")
    private String picture;

    //@Embedded
    //private Address address;


    public User() {
    }

    public User(int id, String gender, String lastname, String firstname, String picture) {
        this.id = id;
        this.gender = gender;
        this.lastname = lastname;
        this.firstname = firstname;
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    //public Address getAddress() {
    //    return address;
    //}

    //public void setAddress(Address address) {
    //    this.address = address;
    //}
}
