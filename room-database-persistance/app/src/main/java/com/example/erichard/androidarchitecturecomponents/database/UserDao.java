package com.example.erichard.androidarchitecturecomponents.database;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.erichard.androidarchitecturecomponents.database.model.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM User WHERE id IN (:userIds)")
    List<User> loadUserById(int[] userIds);

    @Query("SELECT * FROM User WHERE `first-name` LIKE :first AND `last-name` LIKE :last LIMIT 1")
    User findByName(String first, String last);

    //@Query("SELECT * FROM User WHERE address IN (:addresses)")
    //List<User> loadUsersFromAddresses(List<String> addresses);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insetAll(List<User> users);

    @Update
    void update(User user);

    @Delete
    void delete(User user);
}
