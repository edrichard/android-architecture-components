package com.example.erichard.androidarchitecturecomponents;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public final class Parser {

    public static String downloadJSON(final String flux) {
        String result = null;
        HttpURLConnection urlConnexion = null;

        try {
            URL url = new URL(flux);
            urlConnexion = (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(
                    urlConnexion.getInputStream());
            result = readStream(in);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            assert urlConnexion != null;
            urlConnexion.disconnect();
        }

        return result;
    }

    private static String readStream(final InputStream is) throws IOException {
        final int stream = 1000;
        StringBuilder sb = new StringBuilder();
        BufferedReader r =
                new BufferedReader(new InputStreamReader(is), stream);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }
}
