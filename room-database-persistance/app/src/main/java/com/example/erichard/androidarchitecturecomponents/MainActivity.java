package com.example.erichard.androidarchitecturecomponents;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.erichard.androidarchitecturecomponents.database.AppDatabase;
import com.example.erichard.androidarchitecturecomponents.database.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private AppDatabase database;
    private UserAdapter userAdapter;

    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, AppDatabase.DB_NAME).build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UserTask().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class UserTask extends AsyncTask<Void, Void, List<User>> {
        @Override
        protected List<User> doInBackground(Void... voids) {
            List<User> users = database.userDao().getAll();

            if (users.isEmpty()) {
                List<User> list = new ArrayList<>();

                for (int i = 0; i < 20; i++) {
                    String jsonResult = Parser.downloadJSON("https://randomuser.me/api/");
                    User user = new User();

                    if (jsonResult != null) {
                        try {
                            JSONObject root = new JSONObject(jsonResult);

                            JSONArray results = root.getJSONArray("results");

                            for (int j = 0; j < results.length(); j++) {
                                JSONObject u = (JSONObject) results.get(j);

                                user.setGender(u.getString("gender"));

                                JSONObject name = u.getJSONObject("name");
                                user.setFirstname(name.getString("first"));
                                user.setLastname(name.getString("last"));

                                JSONObject picture = u.getJSONObject("picture");
                                user.setPicture(picture.getString("medium"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        list.add(user);
                    }
                }

                database.userDao().insetAll(list);
            }

            return database.userDao().getAll();
        }

        @Override
        protected void onPostExecute(List<User> users) {
            recyclerView.setAdapter(new UserAdapter(users));
        }
    }
}
